# Lane Detection Project #

Implement an algorithm to estimate the parameters of ego lane using an image. 

# TODOs

For each of the images in `./figures/inputs`: 

1. Obtain the edges using edge detector (e.g., canny edge detector or [HED](https://github.com/s9xie/hed)). 
2. Project the edge image to the bird-eye view (observed on top) using a homography transformation. You can estimate the homography transformation parameters by manually providing 4 points correspondence between image plane and the target BEV plane.
3. Implement a method to estimate the lanes by fitting two polynomials (second order polynomials would be enough). Consider placing a proper regularization in the estimation process (e.g., the degree of curvature should be small, two lanes should be parallel to each other, typical width of the lanes are 3.7 meters, etc). 


# Expected Results

We want to obtain the two polynomials that estimates our lane's geometric information. As a way to verify the accuracy, we want to have a qualitative visualization like below:
![Example](figures/example/output_example.png)

